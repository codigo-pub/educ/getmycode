import axios from "axios";


//url d'appel
const URLCODE: string = "https://twini.rocks/simplon/getcode.php";
/**
 * En retour de l'appel http on recoit un json avec une propriete msg de type string, exemple => { msg: 'ok' }
 * Valeurs possibles en retour pour msg :
 *      ok = code trouve
 *      c+ = le code est superieur au code donne
 *      c- = le code est inferieur au code donne
 *      ci = le code donne est invalide (non renseigné ou pas un nombre)
 */

//code a tester
let code: number = 5000;

/**
 * Ce que vous devez faire : 
 *    vous devez implementer un programme qui trouve le bon code
 */

// appelle la page getcode.php avec le code souhaite
axios
  .get(`${URLCODE}?code=${code}`)
  .then(function (response) {
    // recupere la reponse et l'affiche
    console.log("%s => %s", code, response.data.msg);
  })
  .catch(function (error) {
    // affiche si erreur de requete http
    console.log(error);
  })
  .finally(function () {
    // dans tous les cas on passe ici suite a la requete...
  });
