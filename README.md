# getmycode

L'objectif de ce projet est de trouver un code secret sur 5 digits !

Le fichier **main.ts** contient un programme avec une requête http **GET** vers une url donnée.

On donne lors de la requête un nombre via la variable de querystring **code**.

En retour de la requête on récupère une information importante, c'est un json avec une propriété **msg**.

Les valeurs possibles de msg sont : 

 >      ok = code trouve
 >      c+ = le code est superieur au code donné
 >      c- = le code est inferieur au code donné
 >      ci = le code donné est invalide (non renseigné ou pas un nombre)

A vous d'implémenter le code nécessaire pour récupérer rapidement le bon code secret !

**Important : utilisez le débogage !**